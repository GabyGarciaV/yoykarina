package main.java;

import Controller.ClockLogic;
import Controller.ToDoList;

import java.awt.*;
import java.text.SimpleDateFormat;
import javax.swing.*;
import javax.swing.border.LineBorder;

/**
 * This class is responsible for creating the initial user interface.
 *
 * @author NullPointerException
 */

public class StudySpace extends JFrame {

    private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    private final int WIDTH = screenSize.width, HEIGHT = screenSize.height;

    private final String BACKGROUND_HOME = "#373C40", BACKGROUND_HEADER = "#2C3033", WHITE = "#ffffff",
            CELESTE = "#2EA199", GRAY = "#373C40";

    private JPanel generalPanel, headerPanel, leftPanel, rightPanel, centerPanel, menuPanel, musicPanel, todoPanel, pomodoroPanel, clockPanel;
    private JLabel nameApp, iconApp;
    private JButton loginButton;
    private LineBorder line = new LineBorder(Color.decode(CELESTE), 7, true);

    private String logoApp = "img/STUDYSPACELOGO.png";

    /**
     * This constructor method is in charge of building the initial interface of the application.
     */

    public StudySpace() {

        generalPanel();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(WIDTH, HEIGHT);
        this.setTitle("STUDY SPACE");
        this.setIconImage(new ImageIcon(getClass().getResource(logoApp)).getImage());
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setLayout(null);
        this.setVisible(true);
        this.add(generalPanel);

    }

    /**
     * This method is responsible for loading all the elements present in the interface.
     */

    public void generalPanel() {

        header();
        leftPanel();
        rightPanel();
        centerPanel();

        generalPanel = new JPanel();
        generalPanel.setSize(WIDTH, HEIGHT);
        generalPanel.setBackground(Color.decode(BACKGROUND_HOME));
        generalPanel.setLayout(null);
        generalPanel.add(headerPanel);
        generalPanel.add(leftPanel);
        generalPanel.add(centerPanel);
        generalPanel.add(rightPanel);

    }

    /**
     * This method is in charge of creating all the elements inside the header.
     */

    public void header() {

        iconApp = new JLabel(new ImageIcon("src/main/java/img/STUDYSPACELOGO.png"));
        iconApp.setBounds(100, 10, 80, 80);

        nameApp = new JLabel("STUDY SPACE");
        nameApp.setForeground(Color.decode(WHITE));
        nameApp.setFont(new Font("Arial", Font.PLAIN, 40));
        nameApp.setBounds(WIDTH / 2 - 150, 20, 300, 60);

        loginButton = new JButton("LOG IN");
        loginButton.setBackground(Color.decode(CELESTE));
        loginButton.setBounds(WIDTH - 300, 20, 200, 60);

        headerPanel = new JPanel();
        headerPanel.setBounds(0, 0, WIDTH, 100);
        headerPanel.setBackground(Color.decode(BACKGROUND_HEADER));
        headerPanel.setLayout(null);
        headerPanel.add(iconApp);
        headerPanel.add(nameApp);
        headerPanel.add(loginButton);

    }

    /**
     * This method takes care of creating all the elements inside the left pane.
     */

    public void leftPanel() {

        menuPanel = new JPanel();
        menuPanel.setBounds(10, 5, 380, 480);
        menuPanel.setBackground(Color.decode(BACKGROUND_HOME));
        menuPanel.setBorder(line);

        musicPanel = new JPanel();
        musicPanel.setBounds(10, 490, 380, 420);
        musicPanel.setBackground(Color.decode(BACKGROUND_HOME));
        musicPanel.setBorder(line);

        leftPanel = new JPanel();
        leftPanel.setBackground(Color.decode(BACKGROUND_HEADER));
        leftPanel.setBounds(0, 100, 400, 980);
        leftPanel.setLayout(null);
        leftPanel.add(menuPanel);
        leftPanel.add(musicPanel);

    }

    /**
     * This method takes care of creating all the elements inside the right pane.
     */

    public void rightPanel() {

        todoPanel = new JPanel();
        todoPanel.setBounds(10, 5, 380, 480);
        todoPanel.setBackground(Color.decode(BACKGROUND_HOME));
        todoPanel.setBorder(line);
        ToDoList toDoList = new ToDoList(todoPanel);

        pomodoroPanel = new JPanel();
        pomodoroPanel.setBounds(10, 490, 380, 300);
        pomodoroPanel.setBackground(Color.decode(BACKGROUND_HOME));
        pomodoroPanel.setBorder(line);

        clockPanel = new JPanel();
        clockPanel.setBounds(10, 795, 380, 100);
        clockPanel.setBackground(Color.decode(BACKGROUND_HOME));
        clockPanel.setBorder(line);
        ClockLogic clock = new ClockLogic(clockPanel);
        clock.start();

        rightPanel = new JPanel();
        rightPanel.setBackground(Color.decode(BACKGROUND_HEADER));
        rightPanel.setBounds(1520, 100, 400, 980);
        rightPanel.setLayout(null);
        rightPanel.add(todoPanel);
        rightPanel.add(pomodoroPanel);
        rightPanel.add(clockPanel);

    }

    /**
     * This method takes care of creating all the elements within the central panel.
     */

    public void centerPanel() {
        centerPanel = new JPanel();
        centerPanel.setBounds(400, 100, 1060, 920);
        centerPanel.setBackground(Color.decode(GRAY));
    }

    /**
     * This method is responsible for running the interface.
     * @param args It is responsible for collecting and storing values.
     */

    public static void main(String[] args) {
        new StudySpace();
    }

}