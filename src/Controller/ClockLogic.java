package Controller;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * This class manages the clock logic
 * @autor NullPointerException
 */

public class ClockLogic extends Thread {

    private Calendar calendar;
    private String time, day;
    private JLabel dayLabel, timeLabel;
    private SimpleDateFormat dayFormat, timeFormat;
    private JPanel clockPanel;

    public ClockLogic(JPanel clockPanel){
        styleOfTheDay(clockPanel);
        timeStyle(clockPanel);
        this.clockPanel = clockPanel;

    }

    /**
     *   In this method the threads are managed, it is what is going to be executed
     *   in the background like updating the clock.
     */

    @Override
    public void run(){
        boolean currentTime = true;
        while (currentTime){
            setDay(dayLabel,dayFormat);
            clockUpdate(timeFormat, timeLabel, clockPanel);
        }
    }

    /**
     * this method helps me to constantly change the time according to 2 milliseconds difference.
     *
     * @param timeFormat I need a format that connects with the user's calendar.
     * @param timeLabel I need to modify the time with this variable
     * @param panelPrincipal the panel in which to add.
     */

    private void clockUpdate(SimpleDateFormat timeFormat, JLabel timeLabel, JPanel panelPrincipal){
        time = timeFormat.format(Calendar.getInstance().getTime());
        timeLabel.setText(time);
        panelPrincipal.add(timeLabel);
        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This method helps me to know the day according to the user's laptop calendar.
     *
     * @param dayLabel I need to modify the text with this variable
     * @param dayFormat I need a format that connects with the user's calendar.
     * @return the text I constantly need to modify.
     */

    private JLabel setDay(JLabel dayLabel, SimpleDateFormat dayFormat){

        day = dayFormat.format(Calendar.getInstance().getTime());
        dayLabel.setText(day);
        return dayLabel;

    }

    /**
     * This method sets the day style or format for the clock.
     * @param clockPanel the panel to be modified.
     */

    public void styleOfTheDay(JPanel clockPanel){

        dayLabel = new JLabel();
        dayFormat = new SimpleDateFormat("| EEEE |");
        dayLabel.setFont(new Font("SERIF",  Font.ITALIC, 23));
        dayLabel.setForeground(Color.WHITE);
        clockPanel.add(dayLabel);

    }

    /**
     * This method performs the style or format of the time for the clock.
     */

    public void timeStyle(JPanel clockPanel) {

        timeLabel = new JLabel();
        timeFormat = new SimpleDateFormat("HH:mm:ss a");
        timeLabel.setFont(new Font("Ink Free", Font.BOLD + Font.PLAIN, 42));
        timeLabel.setForeground(Color.WHITE);
        clockPanel.add(timeLabel);
        clockUpdate(timeFormat, timeLabel, clockPanel);
    }

}
