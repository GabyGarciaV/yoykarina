package Controller;

import Model.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * This is the class principal of the TO-DO LIST.
 * This class controller the TO-DO LIST.
 *
 * @see JFrame
 */
public class ToDoList extends JFrame{

    private TitleToDO title;
    private AddRemoveButtons addRemoveTask;
    private ListOfTask listOfTask;

    private JButton newTaskButton;
    private JButton clearButton;
    private JPanel todoPanel;

    /**
     * This is the constructor of the class.
     *
     * @param todoPanel
     */
    public ToDoList(JPanel todoPanel){
        this.todoPanel = todoPanel;

        title = new TitleToDO();
        addRemoveTask = new AddRemoveButtons();
        listOfTask = new ListOfTask();

        todoPanel.add(title,BorderLayout.NORTH);
        todoPanel.add(listOfTask,BorderLayout.CENTER);
        todoPanel.add(addRemoveTask,BorderLayout.SOUTH);

        newTaskButton = addRemoveTask.getNewTaskButton();
        clearButton = addRemoveTask.getClearButton();

        addListeners();
    }

    /**
     * This method controllers the functionality of the TO-DO LIST.
     */
    public void addListeners(){
        newTaskButton.addMouseListener(new MouseAdapter(){
            @override
            public void mousePressed(MouseEvent e){
                Task task = new Task();
                listOfTask.add(task);
                listOfTask.updateNumbers();


                task.getDone().addMouseListener(new MouseAdapter(){
                    @override
                    public void mousePressed(MouseEvent e){

                        task.changeState();
                        listOfTask.updateNumbers();
                        revalidate();

                    }
                });
            }
        });

        clearButton.addMouseListener(new MouseAdapter(){
            @override
            public void mousePressed(MouseEvent e){
                listOfTask.removeCompletedTasks();
                repaint();
            }
        });
    }
}

