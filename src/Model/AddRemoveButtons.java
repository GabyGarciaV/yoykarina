package Model;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;

import java.awt.*;


/**
 * This class make the buttons for add and remove the task.
 */
public class AddRemoveButtons extends JPanel{

    JButton addTask;
    JButton clear;

    Border emptyBorder = BorderFactory.createEmptyBorder();

    private final String HEX = "#3c4a49", BACKGROUND_HOME = "#373c40";

    /**
     * This is the constructor for make the buttons of add and remove task.
     */
    public AddRemoveButtons() {
        this.setPreferredSize(new Dimension(320,60));

        addTask = new JButton("Add Task");
        addTask.setBorder(emptyBorder);
        addTask.setFont(new Font("RoxboroughCF",Font.ITALIC, 15));
        addTask.setVerticalAlignment(JButton.BOTTOM);
        addTask.setBackground(Color.decode(HEX));
        addTask.setForeground(Color.white);
        addTask.setBorder(BorderFactory.createEmptyBorder());
        this.setBackground(Color.decode(BACKGROUND_HOME));

        this.add(addTask);

        this.add(Box.createHorizontalStrut(20));//Space between buttons

        clear = new JButton("Clear finished tasks");
        clear.setFont(new Font("RoxboroughCF",Font.ITALIC, 15));
        clear.setBorder(emptyBorder);
        clear.setBackground(Color.decode(HEX));
        clear.setForeground(Color.white);
        this.setBackground(Color.decode(BACKGROUND_HOME));

        this.add(clear);
    }

    /**
     * This method gets the button for add task.
     *
     * @return the button addTask.
     */
    public JButton getNewTaskButton(){
        return addTask;
    }

    /**
     * This method gets the button for clear the task completed.
     *
     * @return the button clear
     */
    public JButton getClearButton() {
        return clear;
    }
}