package Model;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * This class make a task.
 */
public class Task extends JPanel{

    JLabel index;
    JTextField taskName;
    JButton done;

    private boolean checked;
    private final String BACKGROUND_HOME = "#373c40", PURPLE = "#4d4952";

    /**
     * This is the constructor of the class task.
     */
    public Task(){
        this.setPreferredSize(new Dimension(400,20));

        this.setLayout(new BorderLayout());

        checked = false;

        index = new JLabel("");
        index.setPreferredSize(new Dimension(20,20));
        index.setHorizontalAlignment(JLabel.CENTER);
        this.add(index,BorderLayout.WEST);

        done = new JButton(" ");
        done.setPreferredSize(new Dimension(30,20));
        done.setBorder(BorderFactory.createEmptyBorder());
        done.setFocusPainted(false);

        this.add(done,BorderLayout.WEST);

        taskName = new JTextField("  TEST");
        taskName.setBorder(BorderFactory.createEmptyBorder());
        taskName.setBackground(Color.decode(BACKGROUND_HOME));
        taskName.setForeground(Color.WHITE);

        this.add(taskName,BorderLayout.CENTER);

    }

    /**
     * This method puts an index to task each.
     *
     * @param num the num of the star.
     */
    public void changeIndex(int num){
        this.index.setText(num+"");
        this.revalidate();
    }


    /**
     * This method gets the done button of each task for when it is done
     *
     * @return the button done.
     */
    public JButton getDone(){
        return done;
    }

    /**
     * This method gets the status of the task to know if the task is done or not.
     *
     * @return the boolean checked.
     */
    public boolean getState(){
        return checked;
    }

    /**
     * This method changes the color to the purple color of the task when it is done.
     */
    public void changeState(){
        this.setBackground(Color.decode(PURPLE));
        taskName.setBackground(Color.decode(PURPLE));
        checked = true;
        revalidate();
    }


}
