package Model;

import java.awt.*;

import javax.swing.JPanel;

/**
 * This class make the list of task.
 */
public class ListOfTask extends JPanel{

    private final String BACKGROUND_HOME = "#373c40";
    Component[] listItems = this.getComponents();

    /**
     * This is the constructor.
     */
    public ListOfTask(){

        GridLayout layout = new GridLayout(10,1);
        layout.setVgap(5);

        this.setLayout(layout);
        this.setPreferredSize(new Dimension(320,340));
        this.setBackground(Color.decode(BACKGROUND_HOME));
    }

    /**
     * This method changes the index of task each.
     */
    public void updateNumbers(){

        for(int i = 0;i<listItems.length;i++){
            if(listItems[i] instanceof Task){
                ((Task)listItems[i]).changeIndex(i+1);
            }
        }

    }

    /**
     * This method removes the task in status of done.
     */
    public void removeCompletedTasks(){
        for(Component c : getComponents()){
            if(c instanceof Task){
                if(((Task)c).getState()){
                    remove(c);
                    updateNumbers();
                }
            }
        }

    }
}
