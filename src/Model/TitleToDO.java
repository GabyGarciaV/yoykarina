package Model;

import java.awt.*;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class make a title for the widget of the TO-DO LIST
 */
public class TitleToDO extends JPanel{

    JLabel titleText;
    private final String BACKGROUND_HOME = "#373c40";

    /**
     * This is the constructor of the title TO-DO LIST class.
     */
    public TitleToDO(){
        this.setPreferredSize(new Dimension(320,50));

        titleText = new JLabel("TO-DO LIST", JLabel.LEFT);
        titleText.setForeground(Color.white);
        titleText.setPreferredSize(new Dimension(330,50));
        titleText.setFont(new Font("Van Helsing", Font.ITALIC, 20));
        this.setBackground(Color.decode(BACKGROUND_HOME));
        this.add(titleText);
    }
}
